//`timescale 1 ns / 1 ps
module axi4lite_lb#(parameter integer DATA_WIDTH = 32,
	parameter integer ADDR_WIDTH = 4
)(	input s_axi_aclk
,input s_axi_aresetn
,input [ADDR_WIDTH-1:0]s_axi_awaddr
,input [2:0]s_axi_awprot
,input s_axi_awvalid
,output s_axi_awready
,input [DATA_WIDTH-1:0]s_axi_wdata
,input [(DATA_WIDTH/8)-1:0]s_axi_wstrb
,input s_axi_wvalid
,output s_axi_wready
,output [1:0]s_axi_bresp
,output s_axi_bvalid
,input s_axi_bready
,input [ADDR_WIDTH-1:0]s_axi_araddr
,input [2:0]s_axi_arprot
,input s_axi_arvalid
,output s_axi_arready
,output [DATA_WIDTH-1:0]s_axi_rdata
,output [1:0]s_axi_rresp
,output s_axi_rvalid
,input s_axi_rready


,output lb_wvalid
,output [ADDR_WIDTH-1:0] lb_waddr
,output [DATA_WIDTH/8-1:0] lb_wstrb
,output [DATA_WIDTH-1:0] lb_wdata
,output [ADDR_WIDTH-1:0] lb_raddr
,input [DATA_WIDTH-1:0] lb_rdata
,output lb_clk
,output  lb_aresetn
);


wire [DATA_WIDTH-1:0] s_axi_reg_data_out;
reg [ADDR_WIDTH-1:0] awaddr;
reg awready;
reg wready;
reg [1:0] bresp;
reg bvalid;
reg [ADDR_WIDTH-1:0] araddr;
reg arready;
reg [DATA_WIDTH-1:0] rdata;
reg [1:0] rresp;
reg rvalid;


reg aw_en;


assign s_axi_awready = awready;
assign s_axi_wready = wready;
assign s_axi_bresp = bresp;
assign s_axi_bvalid = bvalid;
assign s_axi_arready = arready;
assign s_axi_rdata = rdata;
assign s_axi_rresp = rresp;
assign s_axi_rvalid = rvalid;

always @( posedge s_axi_aclk ) begin
	if ( s_axi_aresetn == 1'b0 ) begin
		awready <= 1'b0;
		aw_en <= 1'b1;
	end
	else begin
		if (~awready && s_axi_awvalid && s_axi_wvalid && aw_en) begin
			awready <= 1'b1;
			aw_en <= 1'b0;
		end
		else if (s_axi_bready && bvalid) begin
			aw_en <= 1'b1;
			awready <= 1'b0;
		end
		else begin
			awready <= 1'b0;
		end
	end
end

always @( posedge s_axi_aclk ) begin
	if ( s_axi_aresetn == 1'b0 ) begin
		awaddr <= 0;
	end
	else begin
		if (~awready && s_axi_awvalid && s_axi_wvalid && aw_en) begin
			awaddr <= s_axi_awaddr;
		end
	end
end

always @( posedge s_axi_aclk ) begin
	if ( s_axi_aresetn == 1'b0 ) begin
		wready <= 1'b0;
	end
	else begin
		if (~wready && s_axi_wvalid && s_axi_awvalid && aw_en ) begin
			wready <= 1'b1;
		end
		else begin
			wready <= 1'b0;
		end
	end
end

wire s_axi_reg_wren = wready && s_axi_wvalid && awready && s_axi_awvalid;


always @( posedge s_axi_aclk )begin
	if ( s_axi_aresetn == 1'b0 ) begin
		bvalid <= 0;
		bresp <= 2'b0;
	end
	else begin
		if (awready && s_axi_awvalid && ~bvalid && wready && s_axi_wvalid) begin
			bvalid <= 1'b1;
			bresp <= 2'b0; // 'okay' response
		end // work error responses in future
		else begin
			if (s_axi_bready && bvalid) begin
				bvalid <= 1'b0;
			end
		end
	end
end

always @( posedge s_axi_aclk ) begin
	if ( s_axi_aresetn == 1'b0 ) begin
		arready <= 1'b0;
		araddr <= {ADDR_WIDTH{1'b0}};
	end
	else begin
		if (~arready && s_axi_arvalid) begin
			arready <= 1'b1;
			araddr <= s_axi_araddr;
		end
		else begin
			arready <= 1'b0;
		end
	end
end

always @( posedge s_axi_aclk ) begin
	if ( s_axi_aresetn == 1'b0 ) begin
		rvalid <= 0;
		rresp <= 0;
	end
	else begin
		if (arready && s_axi_arvalid && ~rvalid) begin
			rvalid <= 1'b1;
			rresp <= 2'b0;
		end
		else if (rvalid && s_axi_rready) begin
			rvalid <= 1'b0;
		end
	end
end

wire s_axi_reg_rden = arready & s_axi_arvalid & ~rvalid;

always @( posedge s_axi_aclk )begin
	if ( s_axi_aresetn == 1'b0 ) begin
		rdata <= 0;
	end
	else begin
		if (s_axi_reg_rden) begin
			rdata <= s_axi_reg_data_out;
		end
	end
end

assign lb_wvalid=s_axi_reg_wren;
assign lb_waddr=awaddr;//s_axi_awaddr;
assign lb_wstrb=s_axi_wstrb;
assign lb_wdata=s_axi_wdata;
assign lb_raddr=araddr;//s_axi_araddr;
assign s_axi_reg_data_out=lb_rdata;
assign lb_clk=s_axi_aclk;
assign lb_aresetn=s_axi_aresetn;
endmodule
