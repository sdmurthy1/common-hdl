module chainreset #(parameter NSTEP=2)
(input clk
,input reset
,output [NSTEP-1:0] resetout
,input [NSTEP-1:0] donecriteria
,input [NSTEP*16-1:0] resetlength
,input [NSTEP*16-1:0] readylength
,input [NSTEP*16-1:0] donelength
,input [NSTEP*16-1:0] resettodonecheck
,input [NSTEP*32-1:0] resettimeout
,output stbdone
,output [NSTEP-1:0] done
,output [NSTEP-1:0] result
,output [3:0] dbstate
,output [3:0] dbnext
,output [15:0] dbshift
,output [NSTEP-1:0] dbresetout
,output [NSTEP-1:0] dbdone
);

//wire [NSTEP-1:0] donecriteria_s;
//alatch #(.DWIDTH(NSTEP)) donecriteriaalatch(.clk(clk),.datain(donecriteria),.dataout(donecriteria_s));


reg [15:0] shift=0;
wire [1-1:0] donecriteria_i=donecriteria>>(shift);
wire [1*16-1:0] resetlength_i=resetlength>>((shift)*16);
wire [1*16-1:0] readylength_i=readylength>>((shift)*16);
wire [1*16-1:0] donecnt_i=donelength>>((shift)*16);
wire [1*16-1:0] resettodonecheck_i=resettodonecheck>>((shift)*16);
wire [1*32-1:0] timeout_i=resettimeout>>((shift)*32);

reg [1-1:0] resetout_i=0;
reg [1-1:0] done_i=0;
reg [1-1:0] donestrobe_i=0;
reg [NSTEP-1:0] resetout_r2=0;
reg [NSTEP-1:0] done_sr=0;
reg [NSTEP-1:0] resetout_sr=0;
reg resetbusy_r=0;
reg resetbusy_d=0;
reg stbdone_r=0;
assign stbdone=stbdone_r;
localparam IDLE=4'h0;
localparam START=4'h1;
localparam WAITBEFORERESET=4'h2;
localparam RESETOUT=4'h3;
localparam WAITAFTERRESET=4'h4;
localparam RESETTING=4'h5;
localparam DONE=4'h6;
localparam TIMEOUT=4'h7;
localparam DONETIME=4'h8;
reg [3:0] state=0;
reg [3:0] next=0;
reg [31:0] cnt=0;
reg reset_d=0;
reg [NSTEP-1:0] resetout_d=0;
reg [NSTEP-1:0] resetout_r=0;
always @(posedge clk) begin
	reset_d<=reset;
	if (~|cnt)
		resetout_d<=resetout_sr;
	resetout_r<=resetout_sr & (~resetout_d);
	resetbusy_d<=resetbusy_r;
	stbdone_r<=~resetbusy_r&resetbusy_d;
end
assign resetout=resetout_r;
always @(posedge clk) begin
	if (reset) begin
		state<=IDLE;
	end
	else begin
		state<=next;
		cnt<=(state==next) ? cnt+(state!=TIMEOUT) : 0;
	end
end
always @(*) begin
	case (state)
		IDLE:	next=reset_d ? START : IDLE;
		START:next=WAITBEFORERESET;
		WAITBEFORERESET: 	next=cnt==readylength_i ? RESETOUT : WAITBEFORERESET;
		RESETOUT: next =cnt==resetlength_i ? WAITAFTERRESET: RESETOUT;
		WAITAFTERRESET: next = cnt == resettodonecheck_i ? RESETTING : WAITAFTERRESET;
		RESETTING : next= donecriteria_i ? DONETIME : (|timeout_i & cnt==timeout_i) ? TIMEOUT : RESETTING;
		DONETIME: next = cnt==donecnt_i ? DONE : ~donecriteria_i ? RESETTING : DONETIME;
		DONE: next = &resetout_sr ? IDLE : START;
		TIMEOUT: next= &resetout_sr ? IDLE : START;
	endcase
end
always @(posedge clk) begin
	if (reset) begin
		shift<=0;
		done_sr<=0;
		resetout_sr<=0;
		resetbusy_r<=0;
	end
	else begin
		case (next)
			IDLE: begin
				resetbusy_r<=0;
			end
			START: begin
				resetbusy_r<=1'b1;
			end

			RESETOUT: begin
				if (~|cnt)
					resetout_sr<={resetout_sr[NSTEP-2:0],1'b1};
			end
			DONE: begin
					done_sr<={1'b1,done_sr[NSTEP-1:1]};
					shift<=shift+1;
			end
			TIMEOUT: begin
					done_sr<={1'b0,done_sr[NSTEP-1:1]};
					shift<=shift+1;
			end
		endcase
	end
end
assign done=done_sr;


reg [3:0] dbstate_r=0;
reg [3:0] dbnext_r=0;
reg [15:0] dbshift_r=0;
reg [NSTEP-1:0] dbresetout_r=0;
reg [NSTEP-1:0] dbdone_r;
always @(posedge clk) begin
	dbshift_r<=shift;
	dbstate_r<=state;
	dbnext_r<=next;
	dbresetout_r<=resetout_r;
	dbdone_r<=done_sr;
end
assign dbshift=dbshift_r;
assign dbstate=dbstate_r;
assign dbnext=dbnext_r;
assign dbresetout=dbresetout_r;
assign dbdone=dbdone_r;

endmodule
