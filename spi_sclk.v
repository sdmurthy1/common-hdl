module spi_sclk
(clk,sck,sck_pre);
parameter TSCKHALF=10;
parameter TSCKW= clog2(TSCKHALF)+1; //tsck is 2^5 time ts
parameter DEBUG="false";
function integer clog2;
    input integer value;
    begin
        value = value-1;
        for (clog2=0; value>0; clog2=clog2+1)
            value = value>>1;
    end
endfunction
parameter SCK_RISING_SHIFT=1;
input clk;
output sck;
output sck_pre;

reg sck_r=0;
reg sck_r_d=0;
reg [TSCKW-1:0] tckcnt=0;
always @(posedge clk) begin
	tckcnt <= (tckcnt==0) ? TSCKHALF : tckcnt-1'b1;
	if (tckcnt==0) begin
		sck_r<=~sck_r;
	end
end
assign sck= SCK_RISING_SHIFT ? sck_r : ~sck_r;
assign sck_pre=(tckcnt==0);
endmodule
