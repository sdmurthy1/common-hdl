import cocotb
from cocotb.triggers import Timer, RisingEdge
import numpy
from matplotlib import pyplot

class cordicg:
    def __init__(self,freq):
        self.freq=freq
        self.opdict=dict(yto0=1,zto0=0)
        self.stage=18
        self.normalizeddelay=3
        self.bufindelay=1
        self.latency=self.stage+self.normalizeddelay+self.bufindelay
        pass

    async def testclk(self,dut):
        a.startclk(tstop=10e-3,dut=dut)
        await Timer(257,units='ns')
        print("testclk")
    async def generate_clock(self,freq,pin,tstop):
        clk_cycle_ns=round(1/freq*1e9/2.0,3)
        for i in range(int(tstop/(clk_cycle_ns*1e-9))):
            pin.value = 0
            await Timer(clk_cycle_ns, units='ns')
            pin.value = 1
            await Timer(clk_cycle_ns, units='ns')
    def startclk(self,tstop,dut):
        cocotb.start_soon(self.generate_clock(freq=self.freq,pin=dut.clk,tstop=tstop))


    def abscheck(self,a,b,tol=10):
        return abs(a-b)<=tol
    async def rp2xy(self,dut,r,p):
        await RisingEdge(dut.clk)
        dut.opin.value=self.opdict['zto0']
        dut.xin.value=r
        dut.yin.value=0
        dut.phasein.value=p
        dut.gin.value=1
        await RisingEdge(dut.clk)
#        dut.opin.value=0
#        dut.xin.value=0
#        dut.yin.value=0
#        dut.phasein.value=0
#        dut.gin.value=0
        await self.delayclk(dut,100)
        await self.delayclk(dut,self.latency)
        xout=dut.xout.value.integer
        yout=dut.yout.value.integer
        pout=dut.phaseout.value.integer
        x=r*numpy.cos(p/2**(self.stage+1)*2*numpy.pi)%2**16
        y=r*numpy.sin(p/2**(self.stage+1)*2*numpy.pi)%2**16
        print('xout',xout,yout,pout)
        print('should be',x,y)
        assert self.abscheck(xout,x)
        assert self.abscheck(yout,y)
        await self.delayclk(dut,self.latency)
    async def xy2rp(self,dut,x,y):
        await RisingEdge(dut.clk)
        dut.opin.value=self.opdict['yto0']
        dut.xin.value=x
        dut.yin.value=y
        dut.phasein.value=0
        dut.gin.value=1
        await RisingEdge(dut.clk)
        dut.opin.value=0
        dut.xin.value=0
        dut.yin.value=0
        dut.phasein.value=0
        dut.gin.value=0
        await self.delayclk(dut,self.latency)
        amp=dut.xout.value.integer
        ph=dut.phaseout.value.integer
        print(amp,abs(x+1j*y))
        assert self.abscheck(amp,abs(x+1j*y))
        print(ph%2**(self.stage+1),(numpy.angle(x+1j*y)/2/numpy.pi*2**(self.stage+1))%2**(self.stage+1))
        assert self.abscheck(ph%2**(self.stage+1),(numpy.angle(x+1j*y)/2/numpy.pi*2**(self.stage+1))%2**(self.stage+1),20)
        await self.delayclk(dut,self.latency)

    async def delayclk(self,dut,n):
        for i in range(n):
            await RisingEdge(dut.clk)




a=cordicg(freq=100e6)
@cocotb.test()
async def testclk(dut):
    await a.testclk(dut)
@cocotb.test()
async def xy2rp2xy(dut):
    await a.testclk(dut)
#    await a.xy2rp(dut,3000,-4000)
#    await a.xy2rp(dut,3000,4000)
#    await a.xy2rp(dut,-3000,4000)
#    await a.xy2rp(dut,-3000,-4000)
    await a.rp2xy(dut,3000,60000)
#    await a.rp2xy(dut,30000,400)
#    await a.rp2xy(dut,30000,50000)
#    await a.rp2xy(dut,30000,400)
#    await a.rp2xy(dut,30000,400)
#    for amp in [100,1000,10000,30000,32767]:
#        for ph in range(0,2**17,100):
#            await a.rp2xy(dut,30000,ph)
