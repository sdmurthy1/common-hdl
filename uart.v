module uart #(parameter DWIDTH=8
,parameter NSTOP=1
,parameter UARTCLK=12000000*64/8
,parameter BAUD=9600 //9600,19200,38400,128000
) (input clk
,input RX
,output TX
,input rst
,input [DWIDTH-1:0] txdata
,input txstart
,output [DWIDTH-1:0] rxdata
,output rxvalid
,output txbusy
,output txready
,output txerror
,output rxbusy
,output rxready
,output rxerror

,output [1:0] txstate
,output [1:0] txnext
,output [5:0] txtxcnt
,output txbaudclk
,output txstop
,output startfromtx
,output rxbaudclk
,output [1:0] rxstate
,output [1:0] rxnext
,output [5:0] rxrxcnt
,output [15:0] rxbaudcnt
,output [15:0] txbaudcnt
,output txline_r
,output rxline_r
);
//reg reset=0;
//always @(posedge clk) begin
//	reset<=rst;
//end
localparam [15:0] BAUDDIV2=UARTCLK/BAUD/2;
uarttx #(.DWIDTH(DWIDTH),.NSTOP(NSTOP))
uarttx (.clk(clk)
,.bauddiv2(BAUDDIV2)
,.rst(rst)
,.data(txdata)
,.start(txstart)
,.txline(TX)
,.busy(txbusy)
,.ready(txready)
,.error(txerror)
,.txstate(txstate)
,.txnext(txnext)
,.txtxcnt(txtxcnt)
,.txbaudclk(txbaudclk)
,.txstop(txstop)
,.startfromtx(startfromtx)
,.txline_r(txline_r)
,.txbaudcnt(txbaudcnt)
);
uartrx #(.DWIDTH(DWIDTH),.NSTOP(NSTOP))
uartrx (.clk(clk)
,.bauddiv2(BAUDDIV2)
,.rst(rst)
,.data(rxdata)
,.rxline(RX)
,.valid(rxvalid)
,.busy(rxbusy)
,.ready(rxready)
,.error(rxerror)
,.rxrxcnt(rxrxcnt)
,.rxnext(rxnext)
,.rxstate(rxstate)
,.rxbaudclk(rxbaudclk)
,.rxbaudcnt(rxbaudcnt)
,.rxline_r(rxline_r)
);
endmodule
