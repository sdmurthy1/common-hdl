interface iflocalbus# (parameter integer DATA_WIDTH = 32,
parameter integer ADDR_WIDTH = 10
,parameter integer WSTRB_WIDTH=1
//,parameter integer CLK_FREQ_HZ=600000000
)();
wire clk;
wire [WSTRB_WIDTH-1:0] wren;
wire rden;
wire rdenlast;
wire [ADDR_WIDTH-1:0] waddr;
wire [DATA_WIDTH-1:0] wdata;
wire [ADDR_WIDTH-1:0] raddr;
wire [DATA_WIDTH-1:0] rdata;
wire rvalid;
wire rvalidlast;
//wire clk;
logic aresetn;
(*dont_touch*)reg [14:0] rden15=0;
(*dont_touch*)reg [14:0] rdenlast15=0;
(*dont_touch*)reg [15*ADDR_WIDTH-1:0] raddr15=0;
wire [15:0] rden16={rden15,rden};
wire [15:0] rdenlast16={rdenlast15,rdenlast};
wire [16*ADDR_WIDTH-1:0] raddr16={raddr15,raddr};
always @(posedge clk) begin
	rden15<=rden16[14:0];
	rdenlast15<=rdenlast16[14:0];
	raddr15<={raddr15[14*ADDR_WIDTH-1:0],raddr};
end
modport lb(input rden,wren,waddr,wdata,clk,aresetn,raddr,rdenlast,rden16,rdenlast16,raddr16
,output rdata,rvalid,rvalidlast
);
modport hw(output rden,wren,waddr,wdata,raddr,aresetn,rdenlast
,input rdata,rvalid,rvalidlast,clk
);
modport setclk(output clk);
endinterface

module lbsetclk(iflocalbus.setclk lb,input clk);
assign lb.clk=clk;
endmodule

module lbconnect(iflocalbus.lb host,iflocalbus.hw slave);
assign host.rdata=slave.rdata;
assign host.rvalid=slave.rvalid;
assign host.rvalidlast=slave.rvalidlast;
assign slave.wdata=host.wdata;
assign slave.rden=host.rden;
assign slave.wren=host.wren;
assign slave.waddr=host.waddr;
assign slave.raddr=host.raddr;;
assign slave.aresetn=host.aresetn;
assign slave.rdenlast=host.rdenlast;
endmodule
module lbpriorityswitch(iflocalbus.lb m,iflocalbus.hw c1,iflocalbus.hw c2);
// same clock domain, switch between c1 and c2, when c1 have tx, ignore c2
always @(posedge m.clk) begin
	if (c1.rvalid) begin
		m.rdata<=c1.rdata;
	end
//	else if (c2.rvalid) begin
//		m.rdata<=c2.rdata;
//	end
//	else begin
//		m.rdata<=0;
//	end
m.rvalid<=c1.rvalid||c2.rvalid;
m.rvalidlast<=c1.rvalidlast||c2.rvalidlast;
c1.wdata<=m.wdata;
c1.rden<=m.rden;
c1.wren<=m.wren;
c1.waddr<=m.waddr;
c1.raddr<=m.raddr;;
c1.aresetn<=m.aresetn;
c1.rdenlast<=m.rdenlast;
c2.wdata<=m.wdata;
c2.rden<=m.rden;
c2.wren<=m.wren;
c2.waddr<=m.waddr;
c2.raddr<=m.raddr;;
c2.aresetn<=m.aresetn;
c2.rdenlast<=m.rdenlast;
end
localparam DEBUG="true";
//`include "ilaswitch.vh"
endmodule



module localbus_mappin#(parameter integer DATA_WIDTH=32
,parameter integer ADDR_WIDTH=10
,parameter integer WSTRB_WIDTH=1
)(iflocalbus.hw lb
,input rden
,input rdenlast
,input [WSTRB_WIDTH-1:0] wren
,input [ADDR_WIDTH-1:0] waddr
,input [DATA_WIDTH-1:0] wdata
,output rvalid
,output rvalidlast
,input [ADDR_WIDTH-1:0] raddr
,output [DATA_WIDTH-1:0] rdata
,input clk
,input aresetn
);
assign rvalid=lb.rvalid;
assign rvalidlast=lb.rvalidlast;
assign lb.rden=rden;
assign lb.rdenlast=rdenlast;
assign lb.wren=wren;
assign lb.waddr=waddr;
assign lb.wdata=wdata;
assign lb.raddr=raddr;
assign rdata=lb.rdata;
//assign lb.clk=clk;
assign lb.aresetn=aresetn;
endmodule
