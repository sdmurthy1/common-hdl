module mdiomaster
(input clk
,input [31:0] clk4ratio
,input mdio_o
,output mdio_i
,output mdio_t
,input mdc
,input rst
,input start
,input opr1w0
,input [4:0] phyaddr
,input [4:0] regaddr
,input [15:0] datatx
,output [31:0] datarx
,output rxvalid
,output busy
);
reg start_r=0;
reg start_rd=0;
wire start1=start_r&~start_rd;
always @(posedge clk) begin
	start_r<=start;
	start_rd<=start_r;
end
localparam DWIDTH=32;
reg [DWIDTH-1:0] datarx_r=0;
wire [DWIDTH-1:0] datatx_w;
assign datatx_w={2'b01,opr1w0?2'b10:2'b01,phyaddr,regaddr,2'b10,datatx};
reg [DWIDTH-1:0] datatxsr=0;
reg [DWIDTH-1:0] datarxsr=0;
wire [1:0] clkstate;
wire stop;
reg mdio_tr=0;
reg mdio_or=0;
reg mdc_r=0;
reg [13:0] cmdtxsr=0;
reg opr=0;
reg [1:0] trsr=0;
reg clkstart=0;


localparam IDLE=3'h0;
localparam START=3'h1;
localparam PRE32=3'h2;
localparam ST_OP_PA5_RA5=3'h3;
localparam TA=3'h4;
localparam D16=3'h5;
localparam STOP=3'h6;
reg [2:0] state=IDLE;
reg [2:0] next=IDLE;
reg [15:0] cnt=0;
always @(posedge clk or posedge rst) begin
	if (rst) begin
		state<=IDLE;
	end
	else begin
		state<=next;
		cnt<=(state==next) ? cnt+ (clkstate==3 & tick1) : 0;
	end
	if (start1) begin
		busy_r<=1'b1;
	end
	else if (stop) begin
		busy_r<=1'b0;
	end
end

always @(*) begin
	case (state)
		IDLE: next= start1? START : IDLE;
		START: next = cnt== 1 ? PRE32 : START;
		PRE32: next = cnt== 32 ? ST_OP_PA5_RA5 : PRE32;
		ST_OP_PA5_RA5: next = cnt==14 ? TA : ST_OP_PA5_RA5;
		TA: next = cnt==2 ? D16 : TA;
		D16: next = cnt==16 ? STOP : D16;
		STOP: next = cnt==1 ? IDLE : STOP;
	endcase
end
always @(posedge clk) begin
	if (rst) begin
		mdio_tr<=1'b1;
		mdio_or<=1'b0;
		mdc_r<=1'b0;
		clkstart<=1'b0;
	end
	else begin
		case (next)
			IDLE: begin
				mdio_tr<=1'b1;
				mdio_or<=1'b0;
				mdc_r<=1'b0;
				clkstart<=1'b0;
			end
			START: begin
				datatxsr<=datatx_w;
				opr<=opr1w0;
				datarxsr<=0;
				mdio_tr<=1'b1;
				mdio_or<=1'b0;
				mdc_r<=1'b0;
				clkstart<=1;
			end
			PRE32: begin
				mdio_tr<=1'b0;
				mdio_or<=1'b1;
				case (clkstate)
					1: if (tick0) begin mdc_r<=1'b1; end
					3: if (tick0) begin mdc_r<=1'b0; end
				endcase
			end
			ST_OP_PA5_RA5,TA,D16: begin
				mdio_tr<=next==ST_OP_PA5_RA5 ? 1'b0 : opr;
				case (clkstate)
					0: if (tick1) begin mdio_or<=datatxsr[DWIDTH-1]; datatxsr<=datatxsr<<1; end
					1: if (tick0) begin mdc_r<=1'b1; end
					2: if (tick0) begin datarxsr<={datarxsr[DWIDTH-2:0],mdio_i}; end
					3: if (tick0) begin mdc_r<=1'b0; end
				endcase
			end
			STOP: begin
				mdio_tr<=1'b1;
				mdio_or<=1'b0;
				mdc_r<=1'b0;
				clkstart<=1'b0;
				datarx_r<=datarxsr;
			end
		endcase
	end
end
assign stop=state==STOP & next==IDLE;//datacnt==DWIDTH+3;
assign datarx=datarx_r;
assign mdio_o=mdio_or;
assign mdio_t=mdio_tr;
assign mdc=mdc_r;
assign scl=mdc_r;
assign rxvalid=stop;
assign busy=busy_r;
endmodule
