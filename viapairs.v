//https://forums.xilinx.com/t5/Welcome-Join/synthesizable-verilog-connecting-inout-pins/td-p/284628
module viapairs #(parameter WIDTH=4,parameter USE="TRUE")(inout [2*WIDTH-1:0] a);
genvar i;
generate
	if (USE=="TRUE") begin
		for (i=0;i<WIDTH;i=i+1) begin
			via via(a[2*i],a[2*i+1]);
			wire [31:0] id=i;
		end
	end
endgenerate
endmodule
