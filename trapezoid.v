// Synthesizes to ??? slices at ??? MHz in XC3Sxxx-4 using XST-??

// Intended for 952 kHz [*] output for APEX laser sync
// MiniCircuits ADT1-1WT transformer just barely makes it --
// typical insertion loss is 1 dB from 1 MHz - 400 MHz
//  (referenced to mid-band loss, 0.3 dB typ)
//  [*] 1300 MHz / 7 / 5 / 39
// Trapezoid shape has larger zero-crossing slope than a sine wave --
// probably a good thing -- and takes far fewer resources than a CORDIC.
// It has smooth phase shift properties after a low-pass analog filter,
// unlike a square wave.

`timescale 1ns / 1ns

module trapezoid(
	input clk,  // timespec 9.0 ns
	input reset,  // active high, synchronous with clk
	output [15:0] trap0,
	output [15:0] trap1,
	input enable,
	input [19:0] phase_step_h,
	input [11:0] phase_step_l,
	input [11:0] modulo
);

// 12-bit modulo supports largest known periodicity in a
// suggested LLRF system, 1427 for JLab.  For more normal
// periodicities, use a multiple to get finer granularity.
// Note that the downloaded modulo control is the 2's complement
// of the mathematical modulus.
// e.g., SSRF IF/F_s ratio 8/11, use
//     modulo = 4096 - 372*11 = 4
//     phase_step_h = 2^20*8/11 = 762600
//     phase_step_l = (2^20*8%11)*372 = 2976
// e.g., Argonne RIA test IF/F_s ratio 9/13, use
//     modulo = 4096 - 315*13 = 1
//     phase_step_h = 2^20*9/13 = 725937
//     phase_step_l = (2^20*9%13)*315 = 945
// e.g., APEX laser sync out/F_s ratio 1/105, use
//     modulo = 4096 - 39*105 = 1
//     phase_step_h = 2^20*1/105 = 9986
//     phase_step_l = (2^20*1%105)*39 = 1794
// See rot_dds_config

// Note that phase_step_h and phase_step_l combined fit in a 32-bit word.
// This is intentional, to allow atomic updates of the two controls
// in 32-bit systems.  Indeed, when modulo==0, those 32 bits can be considered
// a simple binary DDS control.

reg carry=0, reset1=0;
reg [19:0] phase_h=0, phase_step_hp=0;
reg [11:0] phase_l=0;
always @(posedge clk) begin
	{carry, phase_l} <= reset ? 13'b0 : ((carry ? modulo : 12'b0) + phase_l + phase_step_l);
	phase_step_hp <= phase_step_h;
	reset1 <= reset;
	phase_h <= reset1 ? 20'b0 : (phase_h + phase_step_hp + carry);
end

// Trapezoid output
//  0.00 < x < 0.25   (x-0.125)*8
//  0.25 < x < 0.50     +1.0
//  0.50 < x < 0.75   (0.625-x)*8
//  0.75 < x < 1.00     -1.0

// except this output is routed (almost) directly to DAC, so generate offset binary

reg signed [15:0] trapx=0;
always @(posedge clk) case (phase_h[19:17])
	3'b000: trapx <= phase_h[16:1];
	3'b001: trapx <= 16'hffff;
	3'b010: trapx <= 16'hffff;
	3'b011: trapx <= 16'hffff;
	3'b100: trapx <= ~phase_h[16:1];
	3'b101: trapx <= 16'h0000;
	3'b110: trapx <= 16'h0000;
	3'b111: trapx <= 16'h0000;
endcase

// Really want double-data rate output; not there yet.
assign trap0 = enable ? trapx : 16'h8000;
assign trap1 = enable ? trapx : 16'h8000;

endmodule
