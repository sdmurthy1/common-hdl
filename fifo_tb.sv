`timescale 1ns / 1ns
module fifo_tb();
initial begin
	$dumpfile("fifo.vcd");
	$dumpvars(17,fifo_tb);
//$finish();
end
//integer cc=0;
reg sysclk=0;
initial begin
	forever #1 sysclk=~sysclk;
//	for (cc=0; cc<10000; cc=cc+1) begin
//while (1) begin
//		cc=cc+1;
	//	sysclk=0; #2.5;
	//	sysclk=1; #2.5;
//	end
end
localparam FASTPERIOD_2=5;
localparam SLOWPERIOD_2=9;
reg fastclk=0;
initial begin
	forever #(FASTPERIOD_2) fastclk=~fastclk;
end
reg slowclk=0;
initial begin
	forever #(SLOWPERIOD_2) slowclk=~slowclk;
end
reg [31:0] sysclkcnt=0;
always @(posedge sysclk) begin
	sysclkcnt<=sysclkcnt+1;
end
wire wclk=sysclkcnt<4000 ? fastclk : sysclkcnt<4100 ? 0 : sysclkcnt<8000 ? slowclk : sysclkcnt<8100 ? 0 : fastclk;
wire rclk=sysclkcnt<4000 ? slowclk : sysclkcnt<4100 ? 0 : sysclkcnt<8000 ? fastclk : sysclkcnt<8100 ? 0 : fastclk;

reg [31:0] clkcnt=0;
always @(posedge sysclk) begin
	clkcnt<=clkcnt+1;
end
reg [31:0] wrnd=0;
reg [31:0] wclkcnt=0;
always @(posedge wclk) begin
	wclkcnt<=wclkcnt+1;
	wrnd<=$random;
end
reg [31:0] rclkcnt=0;
always @(posedge rclk) begin
	rclkcnt<=rclkcnt+1;
end
localparam DW=8;
wire wen=wclkcnt[5]==1;
wire ren=rclkcnt[7]==0;
wire [DW-1:0] readback;
wire [DW-1:0] wdata=wclkcnt;
wire [DW-1:0] rdata,rdatacore;
wire full,fullcore;
wire empty,emptycore;
localparam RSTCNT=800;
wire rst=sysclkcnt<800;
fifo #(.DW(8),.AW(5),.SIM(1),.SAMECLKDOMAIN(1))
fifo_012 (.wclk(wclk),.rclk(wclk),.din(wdata),.wr_en(wen),.rd_en(ren),.dout(),.empty(),.full()
,.rst(sysclkcnt<RSTCNT+59) // 800+74, 2200+95
);

fifo_1c #(.dw(8),.aw(5))
fifo_1c (.clk(wclk), .din(wdata), .we(wen), .dout(), .re(ren), .full(), .empty(), .depth());
fifo2 #(.dw(8),.aw(5))
fifo2(.rd_clk(rclk),.wr_clk(wclk),.rst(~rst),.din(wdata),.we(wen),.dout(),.re(ren),.full(),.empty(),.wr_level(),.rd_level());

fifocore0 fifocore0(.clk(wclk),.din(wdata),.wr_en(wen),.rd_en(ren),.dout(),.full(),.empty()
,.rst(sysclkcnt<RSTCNT+23)
);
fifocore1 fifocore1(.clk(wclk),.din(wdata),.wr_en(wen),.rd_en(ren),.dout(),.full(),.empty()
,.rst(sysclkcnt<RSTCNT+18)
);
fifocore2 fifocore2(.clk(wclk),.din(wdata),.wr_en(wen),.rd_en(ren),.dout(),.full(),.empty()
,.rst(sysclkcnt<RSTCNT+18)
);
//fifocore3 fifocore3(.clk(wclk),.din(wdata),.wr_en(wen),.rd_en(ren),.dout(),.full(),.empty()
//,.rst(sysclkcnt<RSTCNT+18)
//);
fifocore4 fifocore4(.wr_clk(wclk),.rd_clk(rclk),.din(wdata),.wr_en(wen),.rd_en(ren),.dout(),.full(),.empty()
,.rst(sysclkcnt<RSTCNT+8)
);
fifocore5 fifocore5(.wr_clk(wclk),.rd_clk(rclk),.din(wdata),.wr_en(wen),.rd_en(ren),.dout(),.full(),.empty()
,.rst(sysclkcnt<RSTCNT+6)
);

fifo #(.DW(8),.AW(5),.SIM(1),.SAMECLKDOMAIN(0))
fifo_45(.wclk(wclk),.rclk(rclk),.din(wdata),.wr_en(wen),.rd_en(ren),.dout(),.empty(),.full()
,.rst(sysclkcnt<RSTCNT+93) // 800+74, 2200+95
);
//wire pass=(fifocore4.full==fifocore5.full)&(fifocore4.empty==fifocore5.empty)&(fifocore4.dout==fifocore5.dout);
//wire pass=(fifocore4.full==fifo.full)&(fifocore4.empty==fifo.empty)&(fifocore4.dout==fifo.rdata);
//wire pass=(fifocore0.full==fifo.full)&(fifocore0.empty==fifo.empty)&(fifocore0.dout==fifo.rdata);
endmodule
/*fifo #(.DW(32),.AW(5),.SIM(1))
fifor (.wclk(rclk)
,.rclk(wclk)
,.reset(1'b0)
,.wdata(rdata)
,.wen(~empty)
,.ren(1'b1 || rclkcnt[4]==0)
,.rdata(readback));
reg [31:0] wdata_d=0;
reg [31:0] wdata_d2=0;
reg [31:0] wdata_d3=0;
reg [31:0] wdata_d4=0;
reg [31:0] wdata_d5=0;
reg [31:0] wdata_d6=0;
reg [31:0] wdata_d7=0;
reg [31:0] wdata_d8=0;
reg [31:0] wdata_d9=0;
always @(posedge wclk) begin
	wdata_d<=wdata;
	wdata_d2<=wdata_d;
	wdata_d3<=wdata_d2;
	wdata_d4<=wdata_d3;
	wdata_d5<=wdata_d4;
	wdata_d6<=wdata_d5;
	wdata_d7<=wdata_d6;
	wdata_d8<=wdata_d7;
	wdata_d9<=wdata_d8;
end
wire right=wdata_d7==readback;
*/
