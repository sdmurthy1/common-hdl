interface gmii();
	wire tx_clk;
	wire rx_clk;
	wire [7:0] txd;
	wire tx_en;
	wire tx_er;
	wire [7:0] rxd;
	wire rx_dv;
	wire rx_er;
modport phy(output rxd,rx_dv,rx_er,rx_clk,tx_clk
,input txd,tx_en,tx_er);
modport eth(input tx_clk,rx_clk,rxd,rx_dv,rx_er
,output txd,tx_en,tx_er);
endinterface
