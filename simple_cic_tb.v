`timescale 1ns / 1ns

module simple_cic_tb;

reg clk, fail=0;
integer cc;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("simple_cic.vcd");
		$dumpvars(5,simple_cic_tb);
	end
	for (cc=0; cc<1000; cc=cc+1) begin
		clk=0; #5;
		clk=1; #5;
	end
	$display("%s",fail?"FAIL":"PASS");
end

wire reset=cc==250;
reg gin=0;
reg signed [15:0] din=0;

always @(posedge clk) begin
	din <= -cc*75+200;
	gin <= cc%2==0;
end
wire gout;
wire signed [17:0] dout;
simple_cic #(.DWIN(16),.MAXCICW(4),.DWOUT(18))
dut(.clk(clk)
,.reset(reset)
,.gin(gin)
,.ncic(2'd3)
,.gout(gout)
,.din(din)
,.dout(dout)
);

// No checks yet, will always pass

endmodule
