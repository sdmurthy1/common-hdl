//https://forums.xilinx.com/t5/Welcome-Join/synthesizable-verilog-connecting-inout-pins/td-p/284628
/*module via (w, w)
inout w;
wire w;
endmodule
*/
/*
module via (.a(w1), .b(w2));
inout w1;
inout w2;
wire w;
assign w2=w1;
endmodule
*/
module via#(parameter SIMULATION="")(inout a,inout b);
`ifndef VIAMODULE
	`define VIAMODULE via_v
`endif
`VIAMODULE via(.a(a),.b(b));
endmodule

module via_v(.a(w), .b(w));
inout w;
wire w;
endmodule


module via_sv(inout a,inout b);
alias b=a;
endmodule

/*module via(inout wire a,inout wire b);
tran t1(a,b);
endmodule
*/
