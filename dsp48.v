//(d+b)*a+c
module dsp48(input clk
,input reset
,input [17:0] a
,input [17:0] b
,input [47:0] c
,input [17:0] d
,output [47:0] p
,output [17:0] bout
);
parameter PREADD=1;
reg [17:0] ar=0;
reg [17:0] br=0;
reg [17:0] br2=0;
reg [47:0] cr=0;
reg [17:0] dr=0;
reg [18:0] sum=0;
reg [47:0] mult=0;
reg [47:0] padd=0;
always @(posedge clk) begin
	ar<=a;
	br<=b;
	br2<=br;
	cr<=c;
	dr<=d;
	sum<=PREADD ? br+dr : dr-br;
	mult<=sum*ar;
	padd<=mult+cr;
end
assign p=padd;
assign bout=br2;
endmodule
