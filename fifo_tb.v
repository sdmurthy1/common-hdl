`timescale 1ns / 1ns
module fifo_tb();
reg sysclk=0;
integer cc=0;
initial begin
	$dumpfile("fifo.vcd");
	$dumpvars(17,fifo_tb);
	for (cc=0; cc<10000; cc=cc+1) begin
//while (1) begin
//		cc=cc+1;
		sysclk=0; #2.5;
		sysclk=1; #2.5;
	end
	$finish();
end
localparam WPERIOD_2=5;
localparam RPERIOD_2=37;
reg wclk=0;
initial begin
	forever #(WPERIOD_2) wclk=~wclk;
end
reg rclk=0;
initial begin
	forever #(RPERIOD_2) rclk=~rclk;
end

reg [31:0] clkcnt=0;
always @(posedge sysclk) begin
	clkcnt<=clkcnt+1;
end
reg [31:0] wdata=0;
reg [31:0] wclkcnt=0;
always @(posedge wclk) begin
	wclkcnt<=wclkcnt+1;
	wdata<=$random;
end
reg [31:0] rclkcnt=0;
always @(posedge rclk) begin
	rclkcnt<=rclkcnt+1;
end
wire [31:0] rdata;
wire [31:0] readback;
wire empty;
fifo #(.DW(32),.AW(5),.SIM(1))
fifow (.wclk(wclk)
,.rclk(rclk)
,.reset(1'b0)
,.wdata(wclkcnt) // wdata
,.wen(wclkcnt[6]==1)
,.ren(rclkcnt[9]==0)
,.rdata(rdata)
,.empty(empty));
fifo #(.DW(32),.AW(5),.SIM(1))
fifor (.wclk(rclk)
,.rclk(wclk)
,.reset(1'b0)
,.wdata(rdata)
,.wen(~empty)
,.ren(1'b1 || rclkcnt[4]==0)
,.rdata(readback));
reg [31:0] wdata_d=0;
reg [31:0] wdata_d2=0;
reg [31:0] wdata_d3=0;
reg [31:0] wdata_d4=0;
reg [31:0] wdata_d5=0;
reg [31:0] wdata_d6=0;
reg [31:0] wdata_d7=0;
reg [31:0] wdata_d8=0;
reg [31:0] wdata_d9=0;
always @(posedge wclk) begin
	wdata_d<=wdata;
	wdata_d2<=wdata_d;
	wdata_d3<=wdata_d2;
	wdata_d4<=wdata_d3;
	wdata_d5<=wdata_d4;
	wdata_d6<=wdata_d5;
	wdata_d7<=wdata_d6;
	wdata_d8<=wdata_d7;
	wdata_d9<=wdata_d8;
end
wire right=wdata_d7==readback;
endmodule
