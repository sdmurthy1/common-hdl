`timescale 1ns / 1ns

module source1
#(parameter DEBUG="false"
,parameter WIDTH=16)
(
	input clk,
	input signed [WIDTH-1:0] sin,
	input signed [WIDTH-1:0] cos,
	(* mark_debug = DEBUG *)output signed [WIDTH-1:0] d_outre,
	(* mark_debug = DEBUG *)output signed [WIDTH-1:0] d_outim,
	(* mark_debug = DEBUG *)input signed [WIDTH-1:0] ampi,
	(* mark_debug = DEBUG *)input signed [WIDTH-1:0] ampq
);

// Universal definition; note: old and new are msb numbers, not bit widths.
`define SAT(x,old,new) ((~|x[old:new] | &x[old:new]) ? x[new:0] : {x[old],{new{~x[old]}}})
reg signed [WIDTH:0] d3=0;
reg signed [2*WIDTH-1:0] p1=0, p2=0;  // product registers
wire signed [WIDTH:0] p1s = $signed(p1[2*WIDTH-2:WIDTH-2]);
wire signed [WIDTH:0] p2s = $signed(p2[2*WIDTH-2:WIDTH-2]);
wire signed [WIDTH+1:0] sum = $signed(p1s - p2s + 1);  // one lsb guard, with rounding
wire signed [WIDTH-1:0] ampis=$signed(~|{~ampi[WIDTH-1],ampi[WIDTH-2:0]}? {1'b1,{WIDTH-2{1'b0}},1'b1} : ampi);
wire signed [WIDTH-1:0] ampqs=$signed(~|{~ampq[WIDTH-1],ampq[WIDTH-2:0]}? {1'b1,{WIDTH-2{1'b0}},1'b1} : ampq);
always @(posedge clk) begin
	p1 <= ampis * cos;
	p2 <= ampqs * sin;
	d3 <= $signed(`SAT(sum, WIDTH+1, WIDTH));
end
assign d_outre = $signed(d3[WIDTH:1]+d3[0]);

reg signed [WIDTH:0] d3i=0;
reg signed [2*WIDTH-1:0] p1i=0, p2i=0;  // product registers
wire signed [WIDTH:0] p1si = $signed(p1i[2*WIDTH-2:WIDTH-2]);
wire signed [WIDTH:0] p2si = $signed(p2i[2*WIDTH-2:WIDTH-2]);
wire signed [WIDTH+1:0] sumi = $signed(p1si + p2si + 1);  // one lsb guard, with rounding
always @(posedge clk) begin
	p1i <= ampis * sin;
	p2i <= ampqs * cos;
	d3i <= $signed(`SAT(sumi, WIDTH+1, WIDTH));
end
assign d_outim = $signed(d3i[WIDTH:1]+d3i[0]);
endmodule
