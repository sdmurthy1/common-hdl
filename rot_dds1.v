// Synthesizes to ??? slices at ??? MHz in XC3Sxxx-4 using XST-??

`timescale 1ns / 1ns

module rot_dds(
	input clk,  // timespec 9.0 ns
	input reset,  // active high, synchronous with clk
	output signed [17:0] sina,
	output signed [17:0] cosa,
	input [19:0] phase_step_h,
	input [11:0] phase_step_l,
	input [11:0] modulo
);

// 2^17/1.64676 = 79594, use a smaller value to keep CORDIC round-off
// from overflowing the output
parameter lo_amp = 18'd79590;
wire [20+12+12-1:0] phase_step={phase_step_h,phase_step_l,modulo};
reg [20+12+12-1:0] phase=0;
always @(posedge clk) begin
	phase<=reset? 0: phase+phase_step;
end


cordicg1 #(.WIDTH(18), .NSTAGE(18)) 
trig(.clk(clk), .opin(1'b0),
	.xin(lo_amp), .yin(18'd0), .phasein(phase[43:43-18]),
	.xout(cosa), .yout(sina));

endmodule
