`timescale 1ns / 1ns
module sampgen #(parameter DEBUG="true"
,parameter NSAMP=3
,parameter SAMPWIDTH=14
)
(input clk
,input sample1
,input reset
,output [NSAMP-1:0] sample
,input [NSAMP*SAMPWIDTH-1:0] samp_per
);
reg [NSAMP-1:0] sample_r=0;
genvar isamp;
generate
for (isamp=0;isamp<NSAMP;isamp=isamp+1) begin: eachsamp
	wire [0:0] samp_cnt0;
	reg [SAMPWIDTH-1:0] samp_cnt=0;
	assign samp_cnt0=~|samp_cnt;
	wire [SAMPWIDTH-1:0] isamp_per=samp_per[(isamp+1)*SAMPWIDTH-1:isamp*SAMPWIDTH];
	always @(posedge clk) begin
		if (reset) begin
			samp_cnt <= isamp_per-1;
		end
		else begin
			if (sample1) begin
				samp_cnt <= (samp_cnt0) ? (isamp_per-1'b1) : (samp_cnt-1'b1);
				sample_r[isamp]<= samp_cnt0;
			end
		end
	end
end
endgenerate
assign sample = sample_r;
endmodule
