`timescale 1ns / 1ns

module piloop3_tb;

reg clk;
integer cc;
initial begin
	if ($test$plusargs("vcd")) begin
		$dumpfile("piloop3.vcd");
		$dumpvars(3,piloop3_tb);
	end
	for (cc=0; cc<7000; cc=cc+1) begin
		clk=0; #5;
		clk=1; #5;
	end
	$finish();
end

// Local bus
reg [31:0] lb_data=0;
reg [6:0] lb_addr=0;
reg lb_write=0;

reg signed [17:0] xin=0,ref=0;
wire fake_data=((cc%28)==10);  // resilient; can be spaced out to make picture clearer
wire buf_sync=((cc%448)==2);
reg str_in=0;
always @(posedge clk) begin
	str_in<=fake_data;
	lb_addr <= {7{1'bx}};
	lb_data <= {32{1'bx}};
	lb_write <= 0;
	// if (fake_data) xin<=xin-10;
	// key for register 91:  0x400 to send output 2 to second DAC
	// 0x3 to turn on both use_static bits, 0xc to turn on excite bits.
	// 0x1000 to reverse2
	if (cc==100) begin lb_write<=1; lb_addr<=91;  lb_data<='h403;   end // use_static*
	if (cc==200) begin lb_write<=1; lb_addr<=104; lb_data<=16000;   end // Ki
	if (cc==204) begin lb_write<=1; lb_addr<=105; lb_data<=11000;    end // pole
	if (cc==206) begin lb_write<=1; lb_addr<=106; lb_data<=2000*64; end // Kp
	if (cc==208) begin lb_write<=1; lb_addr<=107; lb_data<=3000;    end // Ki2
	if (cc==210) begin lb_write<=1; lb_addr<=110; lb_data<=1111*64; end // static1
	if (cc==212) begin lb_write<=1; lb_addr<=111; lb_data<=2222*64; end // static2
	if (cc==214) begin lb_write<=1; lb_addr<=92;  lb_data<=200;     end // excite_v
	if (cc==800) xin<=14000;
	if (cc==1200) begin lb_write<=1; lb_addr<=91;  lb_data<='h1404; end // use_static*
	if (cc==6500) xin<=-1000;
	if (cc==6900) begin lb_write<=1; lb_addr<=104; lb_data<=0; end // Ki
end

wire signed [15:0] mdac_val;
wire [2:0] mdac_addr;
wire [2:0] mdac_load;
wire mdac_trig, mdac_busy;
wire amp_ok=1;
wire trace_w, trace_b;
piloop3 #(18,16) pi1(.clk(clk),.sigin(xin),.refin(ref),
	.lb_data(lb_data), .lb_addr(lb_addr), .lb_write(lb_write),
	.strobe_in(str_in), .amp_ok(amp_ok), .buf_sync(buf_sync),
	.rfile_haddr(3'b0),
	.mdac_out(mdac_val), .mdac_addr(mdac_addr), .mdac_load(mdac_load),
	.mdac_trig(mdac_trig), .mdac_busy(mdac_busy),
	.trace_w(trace_w), .trace_b(trace_b));
wire DAC_CLK, DAC_CS;
wire [2:0] DAC_DIN;
wire dac_reconfig=0;
// AD56x4 driver for PLL control
// should match code fragment in top_level/dsp_layer1.v
reg [1:0] mdac_clk_div=0;
always @(posedge clk) mdac_clk_div=mdac_clk_div+1;
ad56x4_driver4 dac123(.clk(clk), .ref_sclk(mdac_clk_div[1]),
	.reconfig(dac_reconfig), .internal_ref(1'b1),
	.sdac_trig(mdac_trig), .busy(mdac_busy),
	.addr1(mdac_addr), .addr2(mdac_addr), .addr3(mdac_addr),
	.voltage1(mdac_val), .voltage2(mdac_val), .voltage3(mdac_val),
	.load1(mdac_load[0]), .load2(mdac_load[1]), .load3(mdac_load[2]),
	.sclk(DAC_CLK), .sdio(DAC_DIN), .csb(DAC_CS));

// Super-simple AD56x4 simulation
reg [23:0] readout0, readout1, readout2;
always @(negedge DAC_CLK) begin
	if (~DAC_CS) readout0 <= {readout0[22:0],DAC_DIN[0]};
	if (~DAC_CS) readout1 <= {readout1[22:0],DAC_DIN[1]};
	if (~DAC_CS) readout2 <= {readout2[22:0],DAC_DIN[2]};
end
always @(posedge DAC_CS) begin
	$display("FPGA sent %x to ad56x4 #0", readout0);  readout0={24{1'bx}};
	$display("FPGA sent %x to ad56x4 #1", readout1);  readout1={24{1'bx}};
	$display("FPGA sent %x to ad56x4 #2", readout2);  readout2={24{1'bx}};
end

always @(negedge clk) begin
	if (str_in) $display("input %d", xin);
	if (mdac_load[0]) $display("output1 %d", mdac_val);
	if (mdac_load[1]) $display("output2 %d", mdac_val);
	if (pi1.mult_str) $display("mult start X=%d", pi1.mult_inx);
	if (pi1.mult_done) $display("mult result %d", pi1.mult_result);
	if (pi1.mult_done) $display("     addend %d", pi1.integ_state_ext);
	if (pi1.add2_done) $display(" add result %d", pi1.add_result);
	if (pi1.rfile_write) $display("rfile_write rf[%d] = %d", pi1.rfile_add,pi1.rfile_wdata);
end

// clear the DPRAM embedded in piloop3
integer ix;
initial begin
	for (ix=0; ix<8; ix=ix+1) pi1.i_coeff.mem[ix]=0;
	for (ix=0; ix<8; ix=ix+1) pi1.rfile.mem[ix]=0;
end

endmodule
